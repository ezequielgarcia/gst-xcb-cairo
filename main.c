#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <gst/gst.h>
#include <gst/app/gstappsink.h>
#include <xcb/xcb.h>
#include <xcb/xcb_event.h>
#include <cairo.h>
#include <cairo-xcb.h>

#define TITLEBAR_HEIGHT 20

static xcb_connection_t *connection;
static xcb_screen_t *screen;
static xcb_window_t window, video_window;

static int width = 320;
static int height = 240;

static int image_width = 320;
static int image_height = 240;

static xcb_visualtype_t *
screen_default_visual(xcb_screen_t *s)
{
    xcb_depth_iterator_t depth_iter = xcb_screen_allowed_depths_iterator(s);

    if(depth_iter.data)
        for(; depth_iter.rem; xcb_depth_next (&depth_iter))
            for(xcb_visualtype_iterator_t visual_iter = xcb_depth_visuals_iterator(depth_iter.data);
                visual_iter.rem; xcb_visualtype_next (&visual_iter))
                if(s->root_visual == visual_iter.data->visual_id)
                    return visual_iter.data;

    return NULL;
}

void configure_notify_handle(xcb_configure_notify_event_t *event)
{
	cairo_t *cr;
	cairo_text_extents_t extents;
	cairo_surface_t *win_surface;
	cairo_surface_t *surface;

	const char *utf8 = "CAMERA NAME";
	double x,y;
	static uint32_t values[2];

	width = event->width;
	height = event->height;
	values[0] = width;
	values[1] = height - TITLEBAR_HEIGHT;

	/* Resize our video window */
	xcb_configure_window(connection,
		video_window,
		XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT, values);

	/* Create cairo surface from window */
	win_surface = cairo_xcb_surface_create(connection,
			window, screen_default_visual(screen),
			event->width, event->height);
	cr = cairo_create (win_surface);
	if (!cr) {
		cairo_surface_destroy (surface);
		return;
	}

	/* Render text */
	cairo_set_source_rgb(cr, 1, 1, 1);
	cairo_select_font_face (cr, "Sans", CAIRO_FONT_SLANT_NORMAL,
				CAIRO_FONT_WEIGHT_NORMAL);
	cairo_set_font_size (cr, 12.0);
	cairo_text_extents (cr, utf8, &extents);
	x = width / 2 -(extents.width / 2 + extents.x_bearing);
	y = TITLEBAR_HEIGHT / 2 - (extents.height / 2 + extents.y_bearing);
	cairo_move_to (cr, x, y);
	cairo_show_text (cr, utf8);

	/* TODO: Render some rects */

	/* Flush seems not needed.. is it because Cairo takes care of it?
	   Or maybe because XCB queue gets filled and dispatches events
	   to X automatically? */
	xcb_flush(connection);

	cairo_destroy (cr);
	cairo_surface_destroy(win_surface);
}

static void
test_cookie(xcb_void_cookie_t cookie, xcb_connection_t *connection, char *err )
{
	xcb_generic_error_t *error = xcb_request_check (connection, cookie);
	if (error) {
		printf ("ERROR: %s : %"PRIu8"\n", err, error->error_code);
		xcb_disconnect(connection);
		exit (-1);
	}
}

GstFlowReturn new_sample(GstAppSink *appsink, gpointer data)
{
	GstSample *sample = gst_app_sink_pull_sample(appsink);
	GstCaps *caps = gst_sample_get_caps(sample);
	GstBuffer *buffer = gst_sample_get_buffer(sample);
	const GstStructure *info = gst_sample_get_info(sample);
	GstMapInfo map;

	cairo_t *cr;
	cairo_text_extents_t extents;
	cairo_surface_t *win_surface;
	cairo_surface_t *surface;
	const char *utf8 = "OFFLINE";
	double x,y;

	/* Map buffer */
	gst_buffer_map (buffer, &map, GST_MAP_READ);

	/* Create cairo surface from data */
	surface = cairo_image_surface_create_for_data(map.data,
		CAIRO_FORMAT_ARGB32,
		image_width, image_height, image_width * 4);
	if (!surface) {
		gst_buffer_unmap(buffer, &map);
		gst_sample_unref (sample);
		return GST_FLOW_ERROR;
	}

	/* Create cairo surface from window */
	win_surface = cairo_xcb_surface_create(connection,
			video_window, screen_default_visual(screen),
			width, height);
	cr = cairo_create (win_surface);
	if (!cr) {
		cairo_surface_destroy (surface);
		gst_buffer_unmap(buffer, &map);
		gst_sample_unref (sample);
		return GST_FLOW_ERROR;
	}

	/* Paint into window surface */
	cairo_save(cr);
	cairo_scale(cr, (double)width / image_width,
			(double)height / image_height);
	cairo_set_operator (cr, CAIRO_OPERATOR_SOURCE);
	cairo_set_source_surface(cr, surface, 0, 0);
	cairo_paint(cr);
	cairo_restore(cr);

	/* Render text */
	cairo_set_source_rgb(cr, 0, 0, 1);
	cairo_select_font_face (cr, "Sans", CAIRO_FONT_SLANT_NORMAL,
				CAIRO_FONT_WEIGHT_NORMAL);
	cairo_set_font_size (cr, 52.0);
	cairo_text_extents (cr, utf8, &extents);
	x = width / 2 -(extents.width / 2 + extents.x_bearing);
	y = height / 2 - (extents.height / 2 + extents.y_bearing);
	cairo_move_to (cr, x, y);
	cairo_show_text (cr, utf8);

	/* TODO: Render some rects */

	/* Flush seems not needed.. is it because Cairo takes care of it?
	   Or maybe because XCB queue gets filled and dispatches events
	   to X automatically? */
	xcb_flush(connection);

	cairo_destroy (cr);
	cairo_surface_destroy(surface);
	cairo_surface_destroy(win_surface);

	/* Unmap buffer, unref sample */
	gst_buffer_unmap(buffer, &map);
	gst_sample_unref (sample);

	return GST_FLOW_OK;
}

static gboolean bus_cb(GstBus *bus, GstMessage *message, gpointer data)
{
	printf("Got %s message\n", GST_MESSAGE_TYPE_NAME (message));
	switch (GST_MESSAGE_TYPE (message)) {
	case GST_MESSAGE_ERROR: {
		GError *err;
		gchar *debug;

		gst_message_parse_error (message, &err, &debug);
		g_print ("Error: %s\n", err->message);
		g_error_free (err);
		g_free (debug);
		break;
	}
	case GST_MESSAGE_EOS:
		/* end-of-stream */
		break;
	default:
		/* unhandled message */
		break;
	}
	/* we want to be notified again the next time there is a message
	 * on the bus, so returning TRUE (FALSE means we want to stop watching
	 * for messages on the bus and our callback should not be called again)
	 */
	return TRUE;
}

int
main(int argc, char **argv)
{
	GstElement *pipeline, *src, *sink;
	GstAppSink *appsink;
	GstStateChangeReturn ret;
	GstBus *bus;
	guint bus_watch_id;

	xcb_generic_event_t *event;
	xcb_gcontext_t gc;
	xcb_void_cookie_t cookie;
	uint32_t values[2];
	uint32_t mask;
	int screen_num;
	int exit, state;
	GError *error = NULL;

	connection = xcb_connect(NULL, &screen_num);
	if (!connection) {
		printf("ERROR: can't connect to an X server\n");
		return -1;
	}

	/* get the current screen */
	xcb_screen_iterator_t iter = xcb_setup_roots_iterator(xcb_get_setup(connection));

	/* we want the screen at index screen_num of the iterator */
	for (int i = 0; i < screen_num; ++i)
		xcb_screen_next(&iter);

	screen = iter.data;
	if (!screen) {
		printf ("ERROR: can't get the current screen\n");
		xcb_disconnect (connection);
		return -1;
	}

	/* create windows */
	window = xcb_generate_id(connection);
	video_window = xcb_generate_id(connection);

	mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
	values[0] = 0;
	values[1] = XCB_EVENT_MASK_KEY_PRESS | XCB_EVENT_MASK_EXPOSURE |
		    XCB_EVENT_MASK_STRUCTURE_NOTIFY;
	cookie = xcb_create_window_checked(connection,
			screen->root_depth,
			window, screen->root,
			0, 0,
			image_width, image_height + TITLEBAR_HEIGHT,
			0, XCB_COPY_FROM_PARENT,
			screen->root_visual,
			mask, values);
	test_cookie(cookie, connection, "can't create window");

	/* No back pixel == No back pixmap? */
	mask = 0;
	cookie = xcb_create_window_checked(connection,
			screen->root_depth,
			video_window, window,
			0, TITLEBAR_HEIGHT,
			image_width, image_height,
			0, XCB_COPY_FROM_PARENT,
			screen->root_visual,
			mask, values);
	test_cookie(cookie, connection, "can't create video window");

	cookie = xcb_map_window_checked(connection, window);
	test_cookie(cookie, connection, "can't map window");

	cookie = xcb_map_window_checked(connection, video_window);
	test_cookie(cookie, connection, "can't map video window");

	gst_init(&argc, &argv);

	if (0) {
		pipeline = gst_pipeline_new("localpipe");
		src = gst_element_factory_make("videotestsrc pattern=ball", NULL);
		sink = gst_element_factory_make("appsink", NULL);
		gst_bin_add_many(GST_BIN(pipeline), src, sink, NULL);
		gst_element_link(src, sink);

	} else {
		gchar *descr = g_strdup("videotestsrc pattern=ball ! "
				"video/x-raw,width=320,height=240,format=BGRx ! "
				"videoconvert ! "
				"appsink name=sink sync=true");
		pipeline = gst_parse_launch (descr, &error);
		sink = gst_bin_get_by_name (GST_BIN (pipeline), "sink");
	}

	appsink = GST_APP_SINK(sink);

	ret = gst_element_set_state(sink, GST_STATE_READY);
	if (ret != GST_STATE_CHANGE_SUCCESS) {
		printf("ERROR: can't set sink state\n");
		xcb_disconnect(connection);
		return -1;
	}

	gst_app_sink_set_drop(appsink, TRUE);
	gst_app_sink_set_max_buffers(appsink, 1);
	GstAppSinkCallbacks callbacks = { NULL, NULL, new_sample };
	gst_app_sink_set_callbacks(appsink, &callbacks, NULL, NULL);

	bus = gst_pipeline_get_bus(GST_PIPELINE(pipeline));
	bus_watch_id = gst_bus_add_watch(bus, bus_cb, NULL);
	gst_object_unref(bus);

	ret = gst_element_set_state(pipeline, GST_STATE_PLAYING);
	if (ret == GST_STATE_CHANGE_FAILURE) {
		printf("ERROR: can't set pipeline state\n");
		gst_element_set_state(pipeline, GST_STATE_NULL);
		xcb_disconnect(connection);
		return -1;
	}

	/* We are ready to show the windows */
	xcb_flush(connection);

	/* Main event loop... this is just used to catch the expose
	 * events and redraw our window.
	 * TODO: Check the ximagesink source for hints on how to
	 * avoid window flickering when resizing the window.
	 * Gstreamer ximagesink works smoothly and there's no flickering.
	 */
	exit = 0;
	state = 0;
	while (!exit) {
		if ((event = xcb_wait_for_event(connection))) {
			xcb_key_release_event_t *kr;

			switch (XCB_EVENT_RESPONSE_TYPE(event)) {
			case XCB_CONFIGURE_NOTIFY:
				configure_notify_handle((void*)event);
				break;
			case XCB_EXPOSE:
				break;
			case XCB_KEY_PRESS:
	                        kr = (xcb_key_release_event_t *)event;
				exit = 1;
				break;
			}
		}
		free(event);
	}

	gst_element_set_state(pipeline, GST_STATE_NULL);
	gst_object_unref(pipeline);

	xcb_free_gc(connection, gc);
	xcb_disconnect(connection);
	return 0;
}
