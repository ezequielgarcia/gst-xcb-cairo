GST_FLAGS=$(shell pkg-config --cflags --libs gstreamer-1.0 gstreamer-app-1.0 gstreamer-video-1.0 xcb cairo cairo-xcb)

app: main.c
	gcc $(GST_FLAGS) main.c -o app

clean:
	rm -f app

